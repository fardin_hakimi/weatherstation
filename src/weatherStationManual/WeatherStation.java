package weatherStationManual;

import java.util.Vector;

public class WeatherStation implements subject{
	private float temperature;
	private float humidity;
	private float pressure;
	Vector<Observer> observers;
	
	public WeatherStation(){
		observers= new Vector<Observer>();
	}

	@Override
	public void registerObservers(Observer o) {
		 observers.add(o);
		
	}

	@Override
	public void removeObservers(Observer o) {
		observers.remove(o);
		
	}
	public void MeasurmentsChanged(float temperature, float humidity, float pressure){
		this.temperature=temperature;
		this.humidity=humidity;
		this.pressure=pressure;
		
		notifyObservers();
		
	}

	@Override
	public void notifyObservers() {
		for(int i=0;i<observers.size();i++){
			Observer o= (Observer) observers.get(i);
			o.update(temperature,humidity,pressure);
		}
		
	}

}
