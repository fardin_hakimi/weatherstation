package weatherStationManual;

public class currentConditions implements Observer,DisplayElement{
	
	private float temperature;
	private float humidity;
	private float pressure;
	WeatherStation weather;
	
	public currentConditions(WeatherStation weather){
		this.weather=weather;
		weather.registerObservers(this);
	}

	@Override
	public void display() {
		
		System.out.println(" Current weather conditions");
		System.out.println("---------------------------");
		
		System.out.println("Temperature: "+temperature+" Humidity: "+
		humidity+" Pressure: "+pressure);
		
	}

	@Override
	public void update(float temperature, float humidity, float pressure) {
		this.temperature=temperature;
		this.humidity=humidity;
		this.pressure=pressure;
		
		display();
		System.out.println("---------------------------");
		
	}

}
