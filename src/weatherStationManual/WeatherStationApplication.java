package weatherStationManual;

public class WeatherStationApplication {
	
	public static void main(String args[]){
	
		WeatherStation weather= new WeatherStation();
		currentConditions current= new currentConditions(weather);
		StatisticsDisplay statistics = new StatisticsDisplay(weather);
		ForecastDisplay forecast= new ForecastDisplay(weather);
		
		weather.MeasurmentsChanged(12, 15, 24);
		weather.MeasurmentsChanged(42, 35, 9);
		weather.MeasurmentsChanged(11, 12, 12);
		weather.MeasurmentsChanged(100, 89, 40);
	}

}
