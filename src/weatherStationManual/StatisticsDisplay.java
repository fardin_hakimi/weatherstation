package weatherStationManual;

public class StatisticsDisplay implements Observer,DisplayElement{
	private WeatherStation weather;
	private float maxTemperature;
	private float minTemparture;
	private float sumofTemperature;
	private float numberofReports;
	
	public StatisticsDisplay(WeatherStation weather){
		this.weather=weather;
		weather.registerObservers(this);
		
		//  set as default values
		maxTemperature=20;
		minTemparture=10;
		sumofTemperature=0;
		numberofReports=0;
	
	}

	@Override
	public void display() {
		float result=sumofTemperature/numberofReports;
       System.out.println(" Weather Statistics");
       System.out.println("-------------------");
       System.out.println("Maximum Temperature: "+maxTemperature+" Minimum Temaperature: "+
        minTemparture+" Average Temperature: "+result);
       
	}

	@Override
	public void update(float temperature, float humidity, float pressure) {
		sumofTemperature+=temperature;
		numberofReports++;
		if(temperature>maxTemperature){
			maxTemperature=temperature;
			
		}
		if (temperature < minTemparture) {
			minTemparture = temperature;
		}

		display();
		System.out.println("-------------------");
		
	}

}
