package weatherStationManual;

public interface subject {
	
	public void registerObservers(Observer o);
	public void removeObservers(Observer o);
	public void notifyObservers();

}