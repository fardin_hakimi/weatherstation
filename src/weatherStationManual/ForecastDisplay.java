package weatherStationManual;

public class ForecastDisplay implements Observer,DisplayElement{
	
	private float Temp;
	private float currentPressure;
	
	WeatherStation weather;
	
	public ForecastDisplay(WeatherStation weather){
		this.weather=weather;
		weather.registerObservers(this);
		// set as default value
		currentPressure=20;
		
	}

	@Override
	public void display() {
		
		System.out.println(" Weather forcast");
		System.out.println("----------------");
		
		if(Temp==currentPressure){
			System.out.println(" The weather will stay the same");
		}
		if(Temp>currentPressure){
			System.out.println("The weather will get colder");
			
		}
		if(Temp<currentPressure){
			System.out.println(" The weather will get warmer");
		}
	 
	}
	
	@Override
	public void update(float temperature, float humidity, float pressure) {
		Temp=currentPressure;
		currentPressure=pressure;
		
		display();
		System.out.println("---------------------------");
		
	}

}
