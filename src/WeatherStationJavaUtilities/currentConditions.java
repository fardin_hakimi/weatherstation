package WeatherStationJavaUtilities;

import java.util.Observable;
import java.util.Observer;

public class currentConditions implements Observer,DisplayElement{
	
	WeatherStation weather;
	Observable OBS;
	
	public currentConditions(WeatherStation w){
		this.weather=w;
		weather.addObserver(this);
	}

	@Override
	public void update(Observable obs, Object arg1){
		this.OBS=obs;
		display();
		System.out.println("----------------------------");
		
	}

	@Override
	public void display() {
		System.out.println(" Current weather conditions");
		System.out.println("---------------------------");
		
		System.out.println("Temperature: "+((WeatherStation)OBS).getTemperature()+
				" Humidity: "+((WeatherStation)OBS).getHumidity()+
				" Pressure: "+((WeatherStation)OBS).getPressure());
		
	}

}
