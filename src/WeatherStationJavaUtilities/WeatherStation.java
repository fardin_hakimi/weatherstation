package WeatherStationJavaUtilities;

import java.util.Observable;

public class WeatherStation extends Observable{
	private float pressure;
	private float humidity;
	private float temperature;
	
	WeatherStation(){
		
	}
	public void MeasurmentsChanged(float temperature, float humidity, float pressure){
		this.temperature=temperature;
		this.humidity=humidity;
		this.pressure=pressure;
		// operations are done at the backstage by these methods
		setChanged();
		notifyObservers();
	}
	public float getPressure(){
		return this.pressure;
	}
	public float getHumidity(){
		return this.humidity;
	}
	public float getTemperature(){
		return this.temperature;
		
	}
	
	

}
