package WeatherStationJavaUtilities;

import java.util.Observable;
import java.util.Observer;

public class StatisticsDisplay implements Observer,DisplayElement{
	private WeatherStation weather;
	Observable OBS;
	private float maxTemperature;
	private float minTemparture;
	private float sumofTemperature;
	private float numberofReports;
	private float temperature;
	
	public StatisticsDisplay(WeatherStation w){
		this.weather=w;
		weather.addObserver(this);
		
	//  set as default values
			maxTemperature=20;
			minTemparture=10;
			sumofTemperature=0;
			numberofReports=0;
	}

	@Override
	public void update(Observable o, Object arg) {
		       this.OBS=o;
		       
		       temperature=((WeatherStation)OBS).getTemperature();
		       
		       sumofTemperature+=temperature;
				numberofReports++;
				if(temperature>maxTemperature){
					maxTemperature=temperature;
					
				}
				if (temperature < minTemparture) {
					minTemparture = temperature;
				}

				display();
				System.out.println("-------------------");
		
	}

	@Override
	public void display() {
		float result=sumofTemperature/numberofReports;
	       System.out.println(" Weather Statistics");
	       System.out.println("-------------------");
	       System.out.println("Maximum Temperature: "+maxTemperature+" Minimum Temaperature: "+
	        minTemparture+" Average Temperature: "+result);
		
	}

}
