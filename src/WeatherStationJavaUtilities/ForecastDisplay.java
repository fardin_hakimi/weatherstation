package WeatherStationJavaUtilities;

import java.util.Observable;
import java.util.Observer;

public class ForecastDisplay implements Observer,DisplayElement{
	Observable OBS;
	WeatherStation weather;
	private float Temp;
	private float currentPressure;
	
	ForecastDisplay(WeatherStation w){
		this.weather=w;
		weather.addObserver(this);
		currentPressure=20;
		
	}

	@Override
	public void update(Observable o, Object arg) {
		           this.OBS=o;
		           Temp=currentPressure;
		   		currentPressure=((WeatherStation)OBS).getPressure();
		           display();
		           System.out.println("----------------------------");
		
	}

	@Override
	public void display() {
		System.out.println(" Weather forcast");
		System.out.println("----------------");
		if(Temp==currentPressure){
			System.out.println(" The weather will stay the same");
		}
		if(Temp>currentPressure){
			System.out.println("The weather will get colder");
			
		}
		if(Temp<currentPressure){
			System.out.println(" The weather will get warmer");
		}
		
	}

}
