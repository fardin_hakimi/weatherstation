package WeatherStationJavaUtilities;

public class WeatherStationApplication {
	
	public static void main(String args[]){
		
		WeatherStation weather= new WeatherStation();
		ForecastDisplay forecast= new ForecastDisplay(weather);
		StatisticsDisplay statistics= new StatisticsDisplay(weather);
		currentConditions current= new currentConditions(weather);
		
		weather.MeasurmentsChanged(87, 77, 56);
		weather.MeasurmentsChanged(12,44, 9);
		weather.MeasurmentsChanged(99, 9, 9);
		
	}

}
